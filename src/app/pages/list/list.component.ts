import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'list',
  template: '<router-outlet></router-outlet>'
})
export class ListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
