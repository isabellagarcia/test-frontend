import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from './api';
import { Router } from '@angular/router';

@Injectable()
export class ProviderAPI extends API {

	constructor(@Inject(HttpClient) authHttp: HttpClient,
        @Inject(Router) router: Router,
    ) {
		super(authHttp, router);
	}

	public get(url) {
		return this._call(url, '', 'get');
	}

	public set_form(url) {
		return this._call(url, '', 'get');
	}

	public update(url, data) {
		return this._call(url, data, 'put');
	}

	public save(url, data) {
		return this._call(url, data, 'post');
	}
	

	public delete(url) {
		return this._call(url, '', 'delete');
	}
}
