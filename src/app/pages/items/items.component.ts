import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'items',
  template: '<router-outlet></router-outlet>'
})
export class ItemsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
