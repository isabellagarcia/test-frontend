import { NbMenuItem } from '@nebular/theme';
import { environment } from '../../environments/environment';

export let MENU_ITEMS: NbMenuItem[] = [
	{
		title: 'Usuários',
		icon: 'nb-person',
		expanded: true,
      	children: [
			{
				title: 'Cadastros',
				link: '/pages/users',
				home: true
			}
		]
	}
];


if(!environment.production) {
	MENU_ITEMS = MENU_ITEMS.concat([
		{
			title: 'DEVELOPER',
			group: true
		},
		{
			title: 'Dashboard',
			icon: 'nb-home',
			link: '/pages/dashboard',

		},
		{
			title: 'Tipografia',
			icon: 'ion-social-sass',
			link: '/pages/typography',
		}
	]);
}
