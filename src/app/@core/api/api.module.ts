import { NgModule } from '@angular/core';
import { ProviderAPI } from './api-provider';
@NgModule({
	imports: [
	],
	providers: [
		ProviderAPI
	],
})

export class APIModule { }
