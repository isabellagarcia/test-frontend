export interface IUser {
	id: string,
	name: string,
	age: number,
	gender: number,
	status: string
}
