import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { NbSecurityModule, NbRoleProvider } from '@nebular/security';
import { of as observableOf } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { throwIfAlreadyLoaded } from './module-import-guard';

// Services
import { AnalyticsService } from './utils/analytics.service';
import { SessionStorageService } from './services/session-storage.service';
import { ProviderAPI } from './api/api-provider';
import { APIModule } from './api/api.module';

@NgModule({
	imports: [
		CommonModule,
	],
	exports: [
		CommonModule,
		BrowserAnimationsModule,
		APIModule
	],
	providers: [
		NbSecurityModule.forRoot({
			accessControl: {
				guest: {
					view: '*',
				},
				user: {
					parent: 'guest',
					create: '*',
					edit: '*',
					remove: '*',
				},
			},
		}).providers,
		AnalyticsService,
		SessionStorageService,
		DatePipe,
	],
	declarations: [],
})

export class CoreModule {}
