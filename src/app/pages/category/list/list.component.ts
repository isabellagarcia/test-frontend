import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'category-list',
  templateUrl: './list.component.html'
})
export class CategoryListComponent implements OnInit {
  public columns;

  public formPage: Array<string> = [];
  constructor() { }

  ngOnInit() {

    this.columns = [{title: 'ID'}, {title: 'Categoria'}, {title: "Tipo"}];
     
    this.formPage = ['pages', 'category', 'form'];
  }

}
