import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ProviderAPI } from '../../../@core/api/api-provider';
import { NbToastrService } from '@nebular/theme';



@Component({
    selector: 'category-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class CategoryFormComponent implements OnInit {
    public formCategory: FormGroup;
    public fields = ['id', 'name'];
    public submitted: boolean = false;
    public id;
    public loading: boolean = false;
    public routine;
    public columns;

    public formPage: Array<string> = [];

    public show: boolean = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private api: ProviderAPI,
        private toast: NbToastrService
    ) {
        this.formCategory = this.formBuilder.group({
            id: [''],
            name: ['', [Validators.required]]
        });
    }

    ngOnInit() {
        this.route.params.subscribe(param => {

            if (param.id) {
                this.id = param.id;
                this.api.set_form('categories/' + param.id).then(result => {
                    for (let item in result) {
                        if (this.fields.indexOf(item) !== -1) {
                            this.formCategory.controls[item].setValue(result[item]);
                        }
                    }
                })
            }
        });
    }

    public cancelClick() {
        this.router.navigate(['pages/category']);
    }

    public submit() {
        this.submitted = true;
        if (this.formCategory.valid) {
            this.loading = true;
            if(this.id){
                let data = {name: this.formCategory.value.name};
                this.api.update('categories/' + this.id, data).then(result => {
                    if(result.id){
                        this.loading = false;
                        this.toast.success('Alterado com sucesso!', 'Sucesso!');
                        this.cancelClick();
                    }else{
                        this.loading = false;
                        this.toast.danger('Houve um erro :(');
                    }

                })
            }else{
                let data = {name: this.formCategory.value.name};
                this.api.save('categories/', data).then(result => {
                    if(result.id){
                        this.loading = false;
                        this.toast.success('Salvo com sucesso!', 'Nova Categoria');
                        this.cancelClick();
                    }else{
                        this.loading = false;
                        this.toast.danger('Houve um erro :(');
                    }
                    
                })  
            }
        }
    }

    public showList(){
        this.show = true;
        this.columns = [{title: 'ID'}, {title: 'Categoria'}, {title: 'Lista'}];
        this.formPage = ['pages',  'list', this.id];
        this.routine = 'categories/' + this.id + '/lists'

    }

}
