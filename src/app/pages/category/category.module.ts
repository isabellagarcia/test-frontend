import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryComponent } from './category.component';
import { Routes, RouterModule } from '@angular/router';
import { CategoryListComponent } from './list/list.component';
import { ThemeModule } from '../../@theme/theme.module';
import { CategoryFormComponent } from './form/form.component';


const routes: Routes = [
	{
		path: '',
    component: CategoryComponent,
    children: [
			{
				path: 'form/:id',
				component: CategoryFormComponent
			},
			{
				path: 'form',
				component: CategoryFormComponent
			},
			{
				path: 'list',
				component: CategoryListComponent
			},
			{
				path: '',
				redirectTo: 'list',
				pathMatch: 'full'
			}
		]
	}
];

@NgModule({
  declarations: [CategoryComponent, CategoryListComponent, CategoryFormComponent],
  imports: [
    CommonModule,
    ThemeModule,
    RouterModule.forChild(routes)
  ]
})
export class CategoryModule { }
