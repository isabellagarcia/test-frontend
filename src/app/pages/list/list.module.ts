import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list.component';
import { Routes, RouterModule } from '@angular/router';
import { ThemeModule } from '../../@theme/theme.module';
import { ListFormComponent } from './form/form.component';



const routes: Routes = [
	{
		path: '',
    component: ListComponent,
    children: [
			{
				path: ':idCategory/:id',
				component: ListFormComponent
			},
			{
				path: ':idCategory',
				component: ListFormComponent
			},
			{
				path: '',
				redirectTo: ':idCategory',
				pathMatch: 'full'
			}
		]
	}
];

@NgModule({
  declarations: [ListComponent, ListFormComponent],
  imports: [
    CommonModule,
    ThemeModule,
    RouterModule.forChild(routes)
  ]
})
export class ListModule { }
