import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ProviderAPI } from '../../../@core/api/api-provider';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'list-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class ListFormComponent implements OnInit {
  public formList: FormGroup;
  public fields = ['id', 'name'];
  public submitted: boolean = false;
  public id;
  public loading: boolean = false;
  public routine;
  public columns;

  public formPage: Array<string> = [];

  public show: boolean = false;
  private idCategory;


  constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private route: ActivatedRoute,
      private api: ProviderAPI,
      private toast: NbToastrService
  ) {
      this.formList = this.formBuilder.group({
          id: [''],
          name: ['', [Validators.required]],
          category: ['']
      });
  }

  ngOnInit() {
      this.route.params.subscribe(param => {
          this.idCategory = param.idCategory;
          this.api.get('categories/' + this.idCategory).then(result => {
            this.formList.controls['category'].setValue(result.name);
          })

          if (param.id) {
              this.id = param.id;
              this.api.set_form('categories/' + param.idCategory + '/lists/' + this.id).then(result => {
                  for (let item in result) {
                      if (this.fields.indexOf(item) !== -1) {
                          this.formList.controls[item].setValue(result[item]);
                      }
                  }
              })
          }
      });
  }

  public cancelClick() {
      this.router.navigate(['pages/category/form/' + this.idCategory]);
  }

  public submit() {
      this.submitted = true;
      if (this.formList.valid) {
          this.loading = true;
          if(this.id){
              let data = {name: this.formList.value.name};
              this.api.update('categories/' + this.idCategory + '/lists/' + this.id, data).then(result => {
                  if(result.id){
                      this.loading = false;
                      this.toast.success('Alterado com sucesso!', 'Sucesso!');
                      this.cancelClick();
                  }else{
                      this.loading = false;
                      this.toast.danger('Houve um erro :(');
                  }

              })
          }else{
              let data = {name: this.formList.value.name};
              this.api.save('categories/'  + this.idCategory + '/lists/', data).then(result => {
                  if(result.id){
                      this.loading = false;
                      this.toast.success('Salvo com sucesso!', 'Nova Lista');
                      this.cancelClick();
                  }else{
                      this.loading = false;
                      this.toast.danger('Houve um erro :(');
                  }
                  
              })  
          }
      }
  }

  public showItens(){
      this.show = true;
      this.columns = [{title: 'ID'}, {title: 'Lista'}, {title: 'Item'}, {title: 'Feito'}];
      this.formPage = ['pages', 'items', this.idCategory, this.id, 'form'];
      this.routine = '/categories/' + this.idCategory + '/lists/' + this.id + '/items'

  }


}
