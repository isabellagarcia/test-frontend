import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/Rx';

export class API {

    //Variáveis auxiliares
    protected __url = environment.api;
    
    constructor(public http: HttpClient,
        private router: Router
        
    ) {}

	public _call(method, body?, requestMethod = 'post') {
		// Retorna a promessa
		return new Promise<any>((resolve, reject) => {
			// Monta os dados
			//if (body && requestMethod != 'get')
				//body = JSON.stringify(body);

			let url = this.__url + '/' + method;
			let request: Promise<any>;

			console.log(url);

			// Analisa o método
			// Monta a requisição/url/params para enviar a api com base no method
			switch (requestMethod.toLowerCase()) {
				case 'get':
					this.http.get(url).subscribe(items =>{
						if(items['error']){
							reject(items)
						}else{
							resolve(items)
						}
                    })
					break;

				case 'delete':
					this.http.delete(url).subscribe(ok =>{
						if(ok['error']){
							reject(ok)
						}else{
							resolve(ok)
						}
                    })
					break;

				case 'put':
					this.http.put(url, body).subscribe(ok =>{
                        if(ok['error']){
							reject(ok)
						}else{
							resolve(ok)
						}
                    })
					break;

				case 'post':
					this.http.post(url, body).subscribe(ok =>{
                        if(ok['error']){
							reject(ok)
						}else{
							resolve(ok)
						}
                    })
					break;

				default:
					reject('Método ' + requestMethod + ' inválido');
            }

		});
	}
}
