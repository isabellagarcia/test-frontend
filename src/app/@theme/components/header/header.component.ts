import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbSidebarService } from '@nebular/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { environment } from '../../../../environments/environment';


@Component({
	selector: 'ngx-header',
	styleUrls: ['./header.component.scss'],
	templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

	@Input() position = 'normal';

	public user: any = {};
	public name: string;

	constructor(private sidebarService: NbSidebarService,
		private router: Router,
		) {

		this.name = "";
	}

	ngOnInit() {
		this.name = environment.name;

	}


	public goToHome() {
		this.router.navigate(['pages']);
	}

	public toggleSidebar(): boolean {
		this.sidebarService.toggle(true, 'menu-sidebar');
		return false;
	}

}
