import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'title-form',
  templateUrl: './title-form.component.html',
  styleUrls: ['./title-form.component.scss']
})
export class TitleFormComponent implements OnInit {
  @Input() title: string = '';
  constructor() { }

  ngOnInit() {
  }

}
