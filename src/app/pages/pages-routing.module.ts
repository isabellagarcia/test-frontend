import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';


const routes: Routes = [{
	path: '',
	component: PagesComponent,
	children: [
		{
			path: 'category',
			loadChildren: './category/category.module#CategoryModule'
		},
		{
			path: 'list',
			loadChildren: './list/list.module#ListModule'
		},
		{
			path: 'items',
			loadChildren: './items/items.module#ItemsModule'
		},
		{
			path: '',
			redirectTo: 'category',
			pathMatch: 'full',
		}
	],
}];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class PagesRoutingModule {}
