import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AngularFireAuth } from 'angularfire2/auth';


@Injectable()
export class SessionStorageService {

	constructor(public firebaseAuth: AngularFireAuth) {}

	private __get(key: string) {
		return JSON.parse(localStorage.getItem(key));
	}

	private __set(key: string, value: string) {
		localStorage.setItem(key, value);
	}

	getUserId() {
		return this.firebaseAuth.auth.currentUser.uid;
	}

	getUserName() {
		return this.firebaseAuth.auth.currentUser.displayName;
	}

	getEmail() {
		return this.firebaseAuth.auth.currentUser.email;
	}

	getUserGroupId(){
		return this.__get('group_id');
	}

	setUserGroupId(value){
		this.__set('group_id', JSON.stringify(value));
	}

	getLanguage() {
		return this.__get('language_default');
	}

	setLanguage(value) {
		this.__set('language_default', JSON.stringify(value));
	}

	resetSession() {
		localStorage.removeItem('group_id');
	}
}
