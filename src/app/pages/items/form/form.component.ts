import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ProviderAPI } from '../../../@core/api/api-provider';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'items-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class ItemsFormComponent implements OnInit {

  public formItem: FormGroup;
  public fields = ['id', 'name', 'done'];
  public submitted: boolean = false;
  public id;
  public loading: boolean = false;
  public routine;
  public columns;

  public formPage: Array<string> = [];

  public show: boolean = false;
  private idCategory;
  private idList;

  constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private route: ActivatedRoute,
      private api: ProviderAPI,
      private toast: NbToastrService
  ) {
      this.formItem = this.formBuilder.group({
          id: [''],
          name: ['', [Validators.required]],
          done: [''],
          category: [''],
          list: ['']
      });
  }

  ngOnInit() {
      this.route.params.subscribe(param => {
          this.idCategory = param.idCategory;
          this.api.get('categories/' + this.idCategory).then(result => {
            this.formItem.controls['category'].setValue(result.name);
          })

          this.idList = param.idList;
          this.api.get('categories/' + this.idCategory + '/lists/' + this.idList).then(result => {
            this.formItem.controls['list'].setValue(result.name);
          })

          if (param.id) {
              this.id = param.id;
              this.api.set_form('categories/' + param.idCategory + '/lists/' + this.id + '/items/' + this.id).then(result => {
                  for (let item in result) {
                      if (this.fields.indexOf(item) !== -1) {
                          this.formItem.controls[item].setValue(result[item]);
                      }
                  }
              })
          }
      });
  }

  public cancelClick() {
      this.router.navigate(['pages/list', this.idCategory, this.idList]);
  }

  public submit() {
      this.submitted = true;
      if (this.formItem.valid) {
          this.loading = true;
          if(this.id){
              let data = {name: this.formItem.value.name, done: this.formItem.value.done};
              this.api.update('categories/' + this.idCategory + '/lists/' + this.idList + '/items/' + this.id, data).then(result => {
                  if(result.id){
                      this.loading = false;
                      this.toast.success('Alterado com sucesso!', 'Sucesso!');
                      this.cancelClick();
                  }else{
                      this.loading = false;
                      this.toast.danger('Houve um erro :(');
                  }

              })
          }else{
              let data = {name: this.formItem.value.name, done: this.formItem.value.done };
              this.api.save('categories/'  + this.idCategory + '/lists/'  + this.idList + '/items', data).then(result => {
                  if(result.id){
                      this.loading = false;
                      this.toast.success('Salvo com sucesso!', 'Novo Item');
                      this.cancelClick();
                  }else{
                      this.loading = false;
                      this.toast.danger('Houve um erro :(');
                  }
                  
              }).catch(e => {
                this.loading = false;
                console.log(e);
              })
              this.loading = false;  
          }
      }
  }

}
