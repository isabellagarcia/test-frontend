
import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';


let counter = 0;

@Injectable()
export class UserService {

	constructor() {}

	getUser(): Observable<any> {
		return observableOf({ name: 'Lee Young', picture: 'assets/images/lee.png' });
	}
}
