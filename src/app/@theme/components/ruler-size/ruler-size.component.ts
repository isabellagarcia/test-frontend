import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'ruler-size',
	templateUrl: './ruler-size.component.html',
	styleUrls: ['./ruler-size.component.scss']
})
export class RulerSizeComponent implements OnInit {

	@Input() text: string;

	constructor() { }

	ngOnInit() { }

}
