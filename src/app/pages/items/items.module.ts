import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemsComponent } from './items.component';
import { Routes, RouterModule } from '@angular/router';
import { ThemeModule } from '../../@theme/theme.module';
import { ItemsFormComponent } from './form/form.component';


const routes: Routes = [
	{
		path: '',
    component: ItemsComponent,
    children: [
			{
				path: ':idCategory/:idList/form/:id',
				component: ItemsFormComponent
			},
			{
				path: ':idCategory/:idList/form',
				component: ItemsFormComponent
			},
			{
				path: '',
				redirectTo: ':idCategory/:idList/form',
				pathMatch: 'full'
			}
		]
	}
];

@NgModule({
  declarations: [ItemsComponent, ItemsFormComponent],
  imports: [
    CommonModule,
    ThemeModule,
    RouterModule.forChild(routes)
  ]
})
export class ItemsModule { }
