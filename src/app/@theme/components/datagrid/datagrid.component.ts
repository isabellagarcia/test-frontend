import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ProviderAPI } from '../../../@core/api/api-provider';


@Component({
  selector: 'datagrid',
  templateUrl: './datagrid.component.html',
  styleUrls: ['./datagrid.component.scss']
})
export class DatagridComponent implements OnInit {
  // Colunas do grid
  @Input() columns: any = {};
  @Input() loading: boolean = false;

  // Nome da rotina na tela
  @Input() routine = '';
  
  // Navigate para o form de edição -> Se não for passado, não aparece botão de inclusão ou edição
  @Input() formPage = [];

  @Input() data;

  @Input() title;

  public columnsLength: number;
  public items: Array<any> = [ ];
  
  constructor( public router: Router, private api: ProviderAPI ) { }

  ngOnInit() {

    this.getItems();
    
  }

  public onEdit(event){
    // Recebe a rota do formulário
		let editNavigatePage = this.formPage;

		// Adiciona o id do registro
    editNavigatePage.push(event[0]);
    

		// Direciona para a tela de formulário
		this.router.navigate(editNavigatePage);
  }

  public onAdd(){

		// Direciona para o formulário
		this.router.navigate(this.formPage);
  }

  public async onDelete(event){
    if(window.confirm('Deseja mesmo apagar esse registro?')){
      try {

				// Exibe o toast de carregamento
				this.loading = true;

				// Faz a requisição
				await this.api.delete(this.routine + '/' + event[0]);
        this.items = [];
				// Atualiza o grid
        this.getItems();

			} catch (error) {

        console.log(error);
        this.loading = false;
			};
			this.loading = false;
    }
  }

  public getItems(){
    this.api.get(this.routine).then(result => {
      for(let item of result){
        this.items.push(Object.values(item));
      }
    })
  }

}
