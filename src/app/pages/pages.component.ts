import { Component } from '@angular/core';
import { NbMenuItem, NbMenuService, NbSidebarService } from '@nebular/theme';

import { MENU_ITEMS } from './pages-menu';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
	selector: 'ngx-pages',
	templateUrl: './pages.component.html'
})
export class PagesComponent {

	public loading = false;

	constructor(
		private router: Router,
		private sidebarService: NbSidebarService,
		private menuService: NbMenuService,
	) {

		this.menuService.onItemSelect().subscribe((_teste) => {
			if (window.innerWidth < 992)
				this.sidebarService.collapse('menu-sidebar');
		});
	}

	ngOnInit() {}


}
